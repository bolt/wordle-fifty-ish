/*
 * This file MUST be included directly in wordle.c and NOT compiled as a
 * separate object.
 *
 * Mainly this is just because we don't want to redefine certain #define's
 * and can't pull them out into their own header without breaking compatability
 * with the pset.
 */

#include "wordle_curses.h"

#include <ncurses.h>
#include <stdbool.h>
#include <string.h>

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

// NOTE: I'd rather not use globals but I'm trying to keep within the bounds
//       of the original pset
// The magic numbers here are the largest amount of guesses and wordsize that
// this program is designed to handle
static char guess_list_g[9][8];
static unsigned int guesses_g, wordsize_g;
static int current_guess_g = -1;
static int padding_y_g = 1;
static int padding_x_g = 2;

static WINDOW *guess_cell_shell[9][8], *guess_cell[9][8];
static WINDOW *astate_win_shell, *astate_win;
static WINDOW *msg_win_shell, *msg_win;

#ifdef DEBUG
static WINDOW *debug_pane;
#define debug_print(...)                    \
    do {                                    \
        wclear(debug_pane);                 \
        wmove(debug_pane, 0, 0);            \
        wprintw(debug_pane, __VA_ARGS__);   \
        wrefresh(debug_pane);               \
    } while (0)
#else
#define debug_print(...)
#endif

#define NAP_REVEAL 300

enum colour_pairs {
    CLR_STD = 1,
    CLR_GREEN,
    CLR_YELLOW,
    CLR_RED,
};

// Internal function prototypes
#define center(line, str) wcenter(stdscr, line, str)
static void wcenter(WINDOW *win, int line, const char *str);
static void cleanup(void);
static void segv_caught(int sig);

void nc_init_ui(int guesses, int wordsize)
{
    guesses_g = guesses;
    wordsize_g = wordsize;

    // Setup signal handling so we can (hopefully) always exit without leaving
    // the terminal in a messed up state
    struct sigaction sa;
    sa.sa_handler = segv_caught;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGSEGV, &sa, NULL) == -1)
        /* TODO: Handle error */;

    initscr();
    noecho();
    cbreak(); /* allow C-c to behave normally */
    keypad(stdscr, TRUE);
    curs_set(0);

    // TODO: Use has_color() and handle non-colour terminals
    start_color();

    // TODO: Can we avoid this for normal text?
    init_pair(CLR_STD, COLOR_WHITE, COLOR_BLACK);
    init_pair(CLR_GREEN, COLOR_BLACK, COLOR_GREEN);
    init_pair(CLR_YELLOW, COLOR_BLACK, COLOR_YELLOW);
    init_pair(CLR_RED, COLOR_BLACK, COLOR_RED);

    int guess_cell_y = 5;
    int guess_cell_x = (COLS - 3 * wordsize) / 2;
    int cell_dim = 3;

    // Create one window for each letter of each guess
    for (int i = 0; i < guesses; i++)
        for (int j = 0; j < wordsize; j++)
        {
            WINDOW **cur_shell = &guess_cell_shell[i][j];
            WINDOW **cur_cell = &guess_cell[i][j];

            *cur_shell = newwin(cell_dim, cell_dim,
                    guess_cell_y + cell_dim * i,
                    guess_cell_x + cell_dim * j);
            if (! *cur_shell)
                /* TODO: handle error */;
            box(*cur_shell, 0, 0);

            *cur_cell = derwin(*cur_shell, 1, 1, 1, 1);
            if (! *cur_cell)
                /* TODO: handle error */;
        }

    int astate_win_h = 3 + padding_y_g * 2;
    int astate_win_w = (10 * 2 - 1) + padding_x_g * 2;
    int astate_win_y = guess_cell_y + cell_dim * guesses + padding_y_g;
    int astate_win_x = (COLS - astate_win_w) / 2;

    astate_win_shell = newwin(astate_win_h, astate_win_w, astate_win_y, astate_win_x);
    if (! astate_win_shell)
        /* TODO: handle error */;
    astate_win = derwin(astate_win_shell, 3, 10 * 2 - 1, padding_y_g, padding_x_g);
    box(astate_win_shell, 0, 0);

    int msg_win_h = 4 + padding_y_g * 2;
    int msg_win_w = (COLS - COLS / 4) + padding_x_g * 2;
    int msg_win_y = astate_win_y - 1;
    int msg_win_x = (COLS - msg_win_w) / 2;

    msg_win_shell = newwin(msg_win_h, msg_win_w, msg_win_y, msg_win_x);
    if (! msg_win_shell)
        /* TODO: handle error */;
    msg_win = derwin(msg_win_shell, 4, COLS - COLS / 4, padding_y_g, padding_x_g);
    box(msg_win_shell, 0, 0);

#ifdef DEBUG
    debug_pane = derwin(stdscr, 3, COLS, LINES - 3, 0);
#endif

    refresh();
    wrefresh(astate_win_shell);

    for (int i = 0; i < guesses; i++)
        for (int j = 0; j < wordsize; j++)
            wrefresh(guess_cell_shell[i][j]);
}

void nc_teardown_ui(void)
{
    getch();

    cleanup();
}

void nc_print_greeting(int guesses, int wordsize)
{
    char title[] = "This is WORDLE50";
    char subtitle_format[] = "You have %i tries to guess the %i-letter word I'm thinking of";
    // NOTE: This is hacky, but should work for the range of values supported
    char subtitle[sizeof(subtitle_format)];
    sprintf(subtitle, subtitle_format, guesses, wordsize);

    attron(COLOR_PAIR(CLR_GREEN) | A_BOLD);
    center(1, title);
    attroff(COLOR_PAIR(CLR_GREEN) | A_BOLD);

    center(3, subtitle);
}

void nc_print_word(char *guess, int wordsize, int status[])
{
    WINDOW **shell_row = guess_cell_shell[current_guess_g];
    WINDOW **cell_row = guess_cell[current_guess_g];

    for (int i = 0; i < wordsize; i++)
    {
        WINDOW *shell = shell_row[i];
        WINDOW *cell = cell_row[i];

        attr_t colour = status[i] == EXACT ? CLR_GREEN :
                        status[i] == CLOSE ? CLR_YELLOW : CLR_RED;

        char c_up = guess[i] & ~0x20;

        wbkgd(shell, COLOR_PAIR(colour));

        wattron(cell, COLOR_PAIR(colour));
        waddch(cell, c_up);
        wattroff(cell, COLOR_PAIR(colour));

        wrefresh(shell);
        napms(NAP_REVEAL);
    }
}

void nc_print_alphabet_state(char *guess, int status[], int wordsize)
{
    static unsigned char state[26] = {};
    static unsigned char print_order[26] = {"qwertyuiopasdfghjklzxcvbnm"};

    // We store the status offset by one so that we can later differentiate
    // between a WRONG state and no state at all
    if (guess)
        for (int i = 0; i < wordsize; i++)
            state[guess[i] - 'a'] = status[i] + 1;

    wclear(astate_win);

    for (int i = 0; i < 26; i++)
    {
        char c = print_order[i];
        char c_up = c & ~0x20;

        attr_t colour = state[c - 'a'] == EXACT + 1 ? CLR_GREEN :
                        state[c - 'a'] == CLOSE + 1 ? CLR_YELLOW :
                        state[c - 'a']              ? CLR_RED : CLR_STD;

        wattron(astate_win, COLOR_PAIR(colour));
        wprintw(astate_win, "%c", c_up);
        wattroff(astate_win, COLOR_PAIR(colour));

        if (i == 9) wmove(astate_win, 1, 1);
        else if (i == 18) wmove(astate_win, 2, 3);
        else waddch(astate_win, ' ');
    }

    wrefresh(astate_win);
}

void nc_print_result(bool won, char *choice)
{
    static char *note[] = {"Genius", "Magnificent", "Impressive", "Splendid", "Great", "Phew"};

    if (won == true)
    {
        wcenter(msg_win, 0, note[current_guess_g]);
        wcenter(msg_win, 1, "You won!");
        wcenter(msg_win, 3, "Press any key to exit...");
    }
    else
    {
        // Hacky but should suffice
        char buf_format[] = "The word was: %s";
        char buf[sizeof(buf_format) + MAX_WORDSZ];
        sprintf(buf, buf_format, choice);

        wcenter(msg_win, 0, "Sorry, better luck next time!");
        wcenter(msg_win, 1, buf);
        wcenter(msg_win, 3, "Press any key to exit...");
    }
    wrefresh(msg_win_shell);
}

/*
 * NOTE: Uses globals:
 *   - current_guess_g is an index into guess_list_g
 *   - current_guess_g is assumed to always be in bounds
 */
char *get_guess(int wordsize)
{
    current_guess_g++;

    WINDOW **cell_row = guess_cell[current_guess_g];
    char *guess = guess_list_g[current_guess_g];
    int pos = 0;

    while (1)
    {
        WINDOW *cell = cell_row[pos];

        int c = getch();

        // Use end of line only when not premature
        if (c == '\n' || c == '\r')
        {
            if (pos < wordsize) continue;
            else break;
        }

        // Handle character deletion
        if ((c == KEY_BACKSPACE || c == 127) && pos)
        {
            pos--;
            guess[pos] = '\0';
            wclear(cell_row[pos]);
            wrefresh(cell_row[pos]);
            continue;
        }

        // Ignore input once we're maxed out
        // ensure users actually provide a guess that is the correct length
        if (pos == wordsize) continue;

        // Enforce lowercase for internal use
        c |= 0x20;

        // Ignore non-alphabetical characters
        if (c < 'a' || c > 'z') continue;

        // Process the new addition
        guess[pos] = c;
        pos++;

        // Display in uppercase
        char c_up = c & ~0x20;
        waddch(cell, c_up);
        wrefresh(cell);
    }

    return guess;
}

void wcenter(WINDOW *win, int line, const char *str)
{
    int width = getmaxx(win);
    int len = strlen(str);
    int indent = (width - len) / 2;

    mvwaddstr(win, line, indent, str);
}

void cleanup(void)
{
    delwin(astate_win);
    delwin(msg_win);

    for (int i = 0; i < guesses_g; i++)
        for (int j = 0; j < wordsize_g; j++)
            delwin(guess_cell_shell[i][j]);

#ifdef DEBUG
    delwin(debug_pane);
#endif
    endwin();
}

void segv_caught(int sig)
{
    cleanup();
    char buf[] = "Segfault caught. Shutting down gracefully.\n";
    write(STDOUT_FILENO, buf, sizeof(buf));
    exit(42);
}
