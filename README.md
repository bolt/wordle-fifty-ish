# Wordinal

A portmanteau of **Word**le in the term**inal**.
This is an `ncurses` version of wordle based on the Harvard CS50x pset.

Part of the reason for odd construction is to retain compatability (for some
unknown reason) with the course; it still passes `check/submit50`.


## Usage

```sh
$ make
$ ./wordle
```

### Dependencies

- ncurses
- libcs50*

\* Only if compiling the course-compatible version (non-curses). See `Makefile`
for an idea of what's expected.


## For CS50 Staff

I am hoping that because the approach in this code should be significantly
unusual compared to actual pset submissions, that it is ok for this repo to be
public.

However, should you still wish for it to be made private let me know.
