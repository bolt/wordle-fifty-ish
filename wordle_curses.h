#ifndef WORDLE_CURSES_H
#define WORDLE_CURSES_H

#include <ncurses.h>
#include <stdbool.h>

void nc_init_ui(int guesses, int wordsize);
void nc_teardown_ui(void);
void nc_print_greeting(int guesses, int wordsize);
void nc_print_word(char *guess, int wordsize, int status[]);
void nc_print_alphabet_state(char *guess, int status[], int wordsize);
void nc_print_result(bool won, char *choice);
char *nc_get_guess(int wordsize);

// Redefine certain functions
#define init_ui(...) nc_init_ui(__VA_ARGS__)
#define teardown_ui(...) nc_teardown_ui(__VA_ARGS__)
#define print_greeting(...) nc_print_greeting(__VA_ARGS__)
#define print_guess_num(...)
#define print_word(...) nc_print_word(__VA_ARGS__)
#define print_alphabet_state(...) nc_print_alphabet_state(__VA_ARGS__)
#define print_result(...) nc_print_result(__VA_ARGS__)
#define get_guess(...) nc_get_guess(__VA_ARGS__)

#endif /* WORDLE_CURSES_H */
