CC ?= clang
override CFLAGS += -Wall -ggdb -O0 -std=c11

.PHONY: all wordinal compat

all: wordinal

wordinal: CFLAGS += -DUSE_NCURSES
wordinal: LDFLAGS += -lncurses

compat: CFLAGS += -I../libcs50/build/include
compat: STATIC += ../libcs50/build/lib/libcs50.a

wordinal compat: wordle

# Whenever compat is not run, include some extra dependencies
ifeq (,$(filter compat,$(MAKECMDGOALS)))
wordle: wordle_curses.c wordle_curses.h
endif

wordle: wordle.c
	$(CC) $(CFLAGS) $< $(STATIC) -o $@ $(LDFLAGS)
