#define _GNU_SOURCE

#ifndef USE_NCURSES
#include <cs50.h>
#else
typedef char * string;
#endif

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

// each of our text files contains 1000 words
#define LISTSIZE 1000

// values for colors and score
// EXACT == right letter, right place
// CLOSE == right letter, wrong place
// WRONG == wrong letter
#define EXACT 2
#define CLOSE 1
#define WRONG 0

// ANSI color codes for boxed in letters
#define GREEN   "\e[38;2;255;255;255;1m\e[48;2;106;170;100;1m"
#define YELLOW  "\e[38;2;255;255;255;1m\e[48;2;201;180;88;1m"
#define RED     "\e[38;2;255;255;255;1m\e[48;2;220;20;60;1m"
#define RESET   "\e[0;39m"

#define BYTE unsigned char

#define MAX_WORDSZ 8

#ifdef USE_NCURSES
/*
 * Yes, we're including a .c directly.
 * Also, order matters; this file relies on definitions made above, because
 * unfortunately they are not provided ina separate header file in the course
 */
#include "wordle_curses.c"
#else
// Redefine certain functions
#define init_ui(...) def_init_ui(__VA_ARGS__)
#define teardown_ui(...) def_teardown_ui(__VA_ARGS__)
#define print_greeting(...) def_print_greeting(__VA_ARGS__)
#define print_guess_num(...) def_print_guess_num(__VA_ARGS__)
#define print_word(...) def_print_word(__VA_ARGS__)
#define print_alphabet_state(...) def_print_alphabet_state(__VA_ARGS__)
#define print_result(...) def_print_result(__VA_ARGS__)
#define get_guess(...) def_get_guess(__VA_ARGS__)
#endif

/*
 * Store metadata for a single word
 * for each letter in the alphabet we use 8 bits to store between 5 and 8 values
 * each bit represents the position on the letter within the word
 * e.g. placecounts['c'-'a'] == 0b00001001
 * indicates that the letter c is in the 1st and 4th place in the word
 * it also indicates that 2 c's are present in the word
 * NOTE: the order is right to left
 */
typedef struct {
    BYTE placecounts[26];
} metadata_s;

/*
 * We use these globals to get around the restriction of not being able to alter
 * provided function signatures
 * See check_word() comments
 */
metadata_s choice_meta_g;
int meta_is_initialised = 0;

// user-defined function prototypes
//string get_guess(int wordsize);
int check_word(string guess, int wordsize, int status[], string choice);
//void print_word(string guess, int wordsize, int status[]);

// helper function prototypes
void def_init_ui(int guesses, int wordsize);
void def_teardown_ui(void);
void def_print_greeting(int guesses, int wordsize);
void def_print_guess_num(int i);
void def_print_word(string guess, int wordsize, int status[]);
void def_print_alphabet_state(string guess, int status[], int wordsize);
void def_print_result(bool won, string choice);
string def_get_guess(int wordsize);

int check_word_(string guess, int wordsize, int status[], metadata_s choice_meta);
string get_string_(const string fmt, ...);

metadata_s get_meta(string word);
BYTE bit_count(BYTE b);
#ifdef DEBUG
void print_meta(metadata_s metadata);
#endif

/* ========================================================================== */

int main(int argc, string argv[])
{
    // ensure proper usage
    if (argc != 2)
    {
        printf("Usage: ./wordle wordsize\n");
        return 1;
    }

    int wordsize = atoi(argv[1]);

    // ensure argv[1] is either 5, 6, 7, or 8 and store that value in wordsize instead
    if (wordsize < 5 || wordsize > 8)
    {
        printf("Error: wordsize must be either 5, 6, 7, or 8\n");
        return 1;
    }

    // open correct file, each file has exactly LISTSIZE words
    char wl_filename[6];
    sprintf(wl_filename, "%i.txt", wordsize);
    FILE *wordlist = fopen(wl_filename, "r");
    if (wordlist == NULL)
    {
        printf("Error opening file %s.\n", wl_filename);
        return 1;
    }

    // load word file into an array of size LISTSIZE
    char options[LISTSIZE][wordsize + 1];

    for (int i = 0; i < LISTSIZE; i++)
    {
        fscanf(wordlist, "%s", options[i]);
    }

    // pseudorandomly select a word for this game
    srand(time(NULL));
    string choice = options[rand() % LISTSIZE];

    // analyse selected word to facilitate scoring
    metadata_s choice_meta = get_meta(choice);


#ifdef DEBUG
    print_meta(choice_meta);
#endif

    // allow one more guess than the length of the word
    int guesses = wordsize + 1;
    bool won = false;

    init_ui(guesses, wordsize);

    // print greeting, using ANSI color codes to demonstrate
    print_greeting(guesses, wordsize);

    string guess = NULL;

    // main game loop, one iteration for each guess
    for (int i = 0; i < guesses; i++)
    {
        // array to hold guess status, initially set to zero
        int status[wordsize];

        // print the current alphabet usage state
        print_alphabet_state(guess, status, wordsize);

        // obtain user's guess
        guess = get_guess(wordsize);

        // set all elements of status array initially to 0, aka WRONG
        for (int j = 0; j < wordsize; j++)
        {
            status[j] = WRONG;
        }

        // Calculate score for the guess
        // NOTE: We directly call our non-wrapped function
        int score = check_word_(guess, wordsize, status, choice_meta);

        print_guess_num(i);

        // Print the guess
        print_word(guess, wordsize, status);

        // if they guessed it exactly right, set terminate loop
        if (score == EXACT * wordsize)
        {
            won = true;
            break;
        }
    }

    // Print the game's result
    print_result(won, choice);

    // Clean up
    // TODO: Make sure this gets run on all exit paths
    // - particularly Ctrl-C and sigsegv
    teardown_ui();

    // that's all folks!
    return 0;
}

/*
 * Wrapper function because our version does not use choice directly, but we
 * can't change the signature due to the course specs
 * Some backend automated testing calls this function directly
 */
int check_word(string guess, int wordsize, int status[], string choice)
{
    // We use a couple of globals to get around the restriction
    if (! meta_is_initialised)
    {
        choice_meta_g = get_meta(choice);
        meta_is_initialised = 1;
    }
    return check_word_(guess, wordsize, status, choice_meta_g);
}

int check_word_(string guess, int wordsize, int status[], metadata_s meta)
{
    int score = 0;

    metadata_s guess_meta = get_meta(guess);

    // compare guess to choice and score points as appropriate, storing points in status
    BYTE combined_exact = 0;
    BYTE combined_close = 0;

    for (int i = 0; i < 26; i++)
    {
        // For a given letter we determine the placement and number, if any,
        // of EXACT matches
        BYTE exact     = meta.placecounts[i] & guess_meta.placecounts[i];

        // We can then determine the placement of all potential CLOSE matches
        // in the guess
        BYTE close_ref = exact ^ guess_meta.placecounts[i];
        BYTE close     = 0;

        // A guess could have more usages of a letter than the target word
        // so we limit the number of positions that can actually be marked
        // as CLOSE, based on the target word
        BYTE max_close_matches =
            bit_count(meta.placecounts[i]) - bit_count(exact);

        // We can now nibble away at our potential matches, if any, and build
        // our actual CLOSE matches
        // We stop when we run out of set bits in our potentials, or when we
        // have exhausted the limit based on the target word
        // NOTE: This could be done as a for loop but this feels more clear
        {
            int bit = 1;
            while (max_close_matches && close_ref)
            {
                if (close_ref & bit)
                {
                    max_close_matches--;
                    close_ref &= ~bit;
                    close |= bit;
                }
                bit <<= 1;
            }
        }

        // We can now simply blend the results of each letter so we can later
        // derive the status for the whole guess word
        combined_exact |= exact;
        combined_close |= close;
    }

    for (int i = wordsize - 1; i >= 0; i--)
    {
        char bit = 1 << i;
        if (combined_exact & bit)
        {
            status[i] = EXACT;
            score += EXACT;
        }
        else if (combined_close & bit)
        {
            status[i] = CLOSE;
            score += CLOSE;
        }
    }

    return score;
}

/* ========================================================================== */

/* additional helper functions */

#ifndef USE_NCURSES
void def_init_ui(int guesses, int wordsize)
{
    /*
     * Do nothing
     * This function is a placeholder to retain compatability with the pset
     * (specifically check/submit50) while allowing alternatives locally
     */
}

void def_teardown_ui(void)
{
    /* Do nothing */
}

void def_print_greeting(int guesses, int wordsize)
{
    printf(GREEN"This is WORDLE50"RESET"\n");
    printf("You have %i tries to guess the %i-letter word I'm thinking of\n", guesses, wordsize);
}

void def_print_guess_num(int i)
{
    printf("Guess %i: ", i + 1);
}

void def_print_word(string guess, int wordsize, int status[])
{
    // print word character-for-character with correct color coding, then reset terminal font to normal
    for (int i = 0; i < wordsize; i++)
    {
        printf("%s%c"RESET, status[i] == EXACT ? GREEN :
                            status[i] == CLOSE ? YELLOW : RED,
                            guess[i]);
    }
    printf("\n");
}

void def_print_alphabet_state(string guess, int status[], int wordsize)
{
    static unsigned char state[26] = {};
    static unsigned char print_order[26] = {"qwertyuiopasdfghjklzxcvbnm"};

    // We store the status offset by one so that we can later differentiate
    // between a WRONG state and no state at all
    if (guess)
        for (int i = 0; i < wordsize; i++)
            state[guess[i] - 'a'] = status[i] + 1;

    putc('\n', stdout);

    for (int i = 0; i < 26; i++)
    {
        char c = print_order[i];
        printf("%s%c"RESET, state[c - 'a'] == EXACT + 1 ? GREEN :
                            state[c - 'a'] == CLOSE + 1 ? YELLOW :
                            state[c - 'a']              ? RED : "",
                            c);
        if (i == 9) printf("\n ");
        else if (i == 18) printf("\n   ");
        else putc(' ', stdout);
    }

    printf("\n\n");
}

void def_print_result(bool won, string choice)
{
    if (won == true)
    {
        printf("You won!\n");
    }
    else
    {
        printf("Sorry, better luck next time!\nThe word was: %s\n", choice);
    }
}

string def_get_guess(int wordsize)
{
    string guess = NULL;

    // ensure users actually provide a guess that is the correct length
    do
    {
        guess = get_string_("Input a %i-letter word: ", wordsize);
    }
    while (strlen(guess) != wordsize);

    for (int i = 0; i < wordsize; i++)
    {
        guess[i] |= 0x20;
    }

    return guess;
}

/*
 * A wrapper around the get_string function to allow for graceful exit on error
 *
 * NOTE: The public facing get_string is a macro, which we have to bypass.
 * This means we can _only_ use the wrapper.
 */
#undef get_string
string get_string_(const string fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);

    string result = get_string(&ap, fmt);

    // Kill program on error
    if (! result)
    {
        fprintf(stderr, "\nError: get_string returned NULL\n");
        exit(1);
    }

    return result;
}
#endif // USE_NCURSES undefined

/*
 * Populate a metadata_s struct with the number and positions of each letter
 * within word
 * Expects:
 * - word to be nul-terminated
 * - word to only contain values a-z
 */
metadata_s get_meta(string word)
{
    metadata_s m = {};

    for (int i = 0; word[i]; i++)
    {
        m.placecounts[word[i] - 'a'] |= 1 << i;
    }

    return m;
}

/*
 * There are many ways to solve this. This one isn't awful.
 * NOTE: Would be better if we could guarantee output of a single instruction
 * where such is available (most modern machines).
 *
 * We only need to handle 8-bits
 */
BYTE bit_count(BYTE b)
{
    BYTE count = 0;
    while (b)
    {
        count++;
        b &= b - 1;
    }
    return count;
}

#ifdef DEBUG
void print_meta(metadata_s m)
{
    for (int i = 0; i < 26; i++)
    {
        printf("%-4c%s", 'a'+i, i != 25 ? " | " : "\n");
    }
    for (int i = 0; i < 26; i++)
    {
        printf("0x%02x%s", m.placecounts[i], i != 25 ? " | " : "\n");
    }
}
#endif
